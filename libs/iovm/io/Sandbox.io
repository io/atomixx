Sandbox do(
    docSlot("printCallback(string)", 
     "default implementation is; method(string, string print)")
    printCallback := method(string, string print)
)